package lernen;

import java.util.Scanner;

public class Gaeste {
	static boolean validate(int value, int min, int max) {
		return min <= value && value <= max;
	}
	
	public static void main (String[] args) {
		try (Scanner scan = new Scanner(System.in)) {
			boolean weiter = true;
			
			System.out.println("Bitte gib die Anzahl der aktuell angemeldeten Gäste ein");
			int gaeste = scan.nextInt(); // FIXME test 0 <= gaeste  ?
			
			while(weiter) {
				System.out.println("Gast anmelden(1) Gast abmelden(2) Programm beenden(3)");
				int eingabe = scan.nextInt(); // FIXME eingabe {1, 2, 3}?
				
				if(eingabe == 1) {
					/**
					 * c = a + b
					 * - MAX_INT <= a + b
					 * - MAX_INT - a >= b
					 */
					final int min = 1;					
					final int max = Integer.MAX_VALUE-gaeste;
					System.out.println("Wie viele Gäste möchtest du anmelden ["+min+".."+max+"]?");
					final int uebergabe = scan.nextInt();
					if( !validate(uebergabe, min, max) ) {
						System.out.println("Falsche Eingabe '"+uebergabe+"'!");
					} else {
						gaeste = gaeste + uebergabe;
						System.out.println("Du hast " + uebergabe + " Gäste angemeldet");
						System.out.println("Es sind jetzt " + gaeste + " angemeldet");
					}
				}
				
				if(eingabe == 2) {
					System.out.println("Wie viele Gäste möchtest du abmelden?");
					int uebergabe = scan.nextInt();
					gaeste = gaeste - uebergabe; // FIXME test 0 <= gaeste && gaeste <= MAX_INT ?
					System.out.println("Du hast " + uebergabe + " Gäste abgemeldet");
					System.out.println("Es sind jetzt " + gaeste + " a10ngemeldet");
				}
				
				if(eingabe == 3) {
					System.out.println("Möchtest du das Programm wirklich beenden? Ja(1), Nein(´2)");
					int abfrage = scan.nextInt(); // FIXME abfrage {1, 2}?
					
					if(abfrage == 1) {
						System.out.println("Das Programm wurde beendet!");
						weiter = false;
						
					}
					
				}
			}
		} // closes scan instance
		
	}
}