import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

/**
 * Siehe Logik, JUC2 02.01 Logic01
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class Test0201Logik01 {

	/**
	 * Returns the result of: A ∧ B
	 */
	boolean und(boolean a, boolean b) {
		return a && b;
	}
	
	/**
	 * Returns the result of: A ⊕ B := A ∧ ¬B ∨ ¬A ∧ B
	 * 
	 * logisch-nicht operator ¬ -> !
	 */
	boolean xor(boolean a, boolean b) {
		return a && !b || !a && b;
	}	
	
	@Test
	void test00() {
		// bool'sche variable, logik, wahr (true, 1) oder falsch (false, 0)
		final boolean T = true;  // immutable (konstante) booleans
		final boolean F = false; // ..
		
		// logisch-und operator ∧ -> '&&'
		Assertions.assertEquals(false, F && F); 
		Assertions.assertEquals(false, F && T);
		Assertions.assertEquals(false, T && F);
		Assertions.assertEquals(true,  T && T);
		// logisch-und operator ∧ -> '&&' .. mittels funktion und(a, b)
		Assertions.assertEquals(false, und(F, F)); 
		Assertions.assertEquals(false, und(F, T));
		Assertions.assertEquals(false, und(T, F));
		Assertions.assertEquals(true,  und(T, T));
		
		// logisch-oder operator ∨ -> '||'
		Assertions.assertEquals(false, F || F); 
		Assertions.assertEquals(true,  F || T);
		Assertions.assertEquals(true,  T || F);
		Assertions.assertEquals(true,  T || T);
		
		// logisch-exclusive-oder (xor) ⊕
		Assertions.assertEquals(false, xor(F, F)); 
		Assertions.assertEquals(true,  xor(F, T));
		Assertions.assertEquals(true,  xor(T, F));
		Assertions.assertEquals(false, xor(T, T));
	}
	
	@Test
	void test01() {
	}

	@Test
	void test02() {		
	}
}
